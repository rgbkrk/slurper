Slurper
==============

Slurper is a scraper for YouTube accounts that pulls user-entered accounts off the page.

Slurper makes use of:

* Mechanize - Get the data
* BeautifulSoup - Extract it
* SQLAlchemy - Database it
* PiCloud - Do it all distributed
