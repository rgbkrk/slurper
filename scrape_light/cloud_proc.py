#!/usr/bin/env python

# -*- coding: utf-8 -*-

import cloud
from cloud import CloudException
import scrape

import codecs

def join_results(results, users):
    '''
    Processes all the results and puts them in a dictionary where

        user[i] -> result[i] # Note that result type is set

    If result[i] had an error, user[i] gets placed in a list of errored runs
    and user[i] -> set()

    >>> newtubes,cloud_errs,empties = join_results(results,users)

    '''
    newtubes = dict()
    cloud_errs = []
    empties = []

    if(not cloud.running_on_cloud()):
        print "Retrieved results"

    for ii,result in enumerate(results):
        username = users[ii]
        if(type(result) == dict):
            if(len(result) > 0):
                # Gets newtubes['username'] if it exists, otherwise we get an
                # empty dict
                links = newtubes.setdefault(username, dict())
                # username -> dict
                # We update the contents of that dict
                links.update(result)
            else:
                # Empty set for the empty results
                newtubes[username] = dict()
                empties.append(username)
        else:
            cloud_errs.append(username)

        # Without a doubt, we want to make sure that every username has an
        # entry
        newtubes.setdefault(username,dict())
    return newtubes, cloud_errs, empties

def get_ranges(tubers,split=10000):
    if(len(tubers) <= 1):
        raise ValueError("Tubers should be bigger than 1 entry...")

    batch_starts = range(0,len(tubers), split)
    batch_starts.append(len(tubers))

    batch_ranges = [(batch_starts[ii],batch_starts[ii+1]+1) for ii,el in
            enumerate(batch_starts) if ii < len(batch_starts)-1]

    return batch_ranges

def farm_batches(tuber_batches, batch_ranges,scraper=scrape.scrape_guess):
    jids = [None]*len(batch_ranges)

    if(not cloud.running_on_cloud()):
        print("Sending work to the cloud") # I can't believe I just said that...

    for ii,(start,end) in enumerate(batch_ranges):
        if(not cloud.running_on_cloud()):
            print("Mapping scraped_%i" % ii)

        label = 'scraped_%02d' % ii
        jids[ii] = cloud.map(scraper, tuber_batches[ii],
                                _type='s1', _label=label)
    return jids

def retrieve_all(tuber_batches, batch_ranges, jids):

    all_tubes = {}
    all_errs = []
    all_empties = []

    for ii,(start,end) in enumerate(batch_ranges):
        results = cloud.result(jids[ii], ignore_errors=True)
        newtubes, cloud_errs, empties = join_results(results, tuber_batches[ii])

        # The user runs should be unique here, so this is
        # simply filling in new youtubers
        all_tubes.update(newtubes)
        all_errs.extend(cloud_errs)
        all_empties.extend(empties)

    return all_tubes, all_errs, all_empties

def split_scrape(tubers,split=10000,scraper=scrape.scrape_guess):
    batch_ranges = get_ranges(tubers,split)
    tuber_batches = [None]*len(batch_ranges)

    for ii,(start,end) in enumerate(batch_ranges):
        tuber_batches[ii] = tubers[start:end]

    if(not cloud.running_on_cloud()):
        print("Farming Batches...")
    jids = farm_batches(tuber_batches,batch_ranges,scraper=scraper)

    if(not cloud.running_on_cloud()):
        print("Farmed!")
        print("Jids: %r" % jids)

    tubes, errs, empties = retrieve_all(tuber_batches, batch_ranges, jids)

    return tubes, errs, empties, jids

def full_go():
    from users import youtubers
    return split_scrape(youtubers,split=10000)

