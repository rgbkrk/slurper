#!/usr/bin/env python

# -*- coding: utf-8 -*-

import collections
import itertools

import urllib
import urllib2
from urllib2 import urlparse

from lxml import etree
from lxml import objectify

#TODO Set up functional tests that operate off of the examples directory
#     IDEA: Make the base_url relative to a local path to reach the examples

xpath_gplus =\
        "//*[contains(@class,'secondary-pane')]" +\
        "/.//a["+\
            "img[contains(@class,'google-plus-linked-name-icon')]"+\
        "]/@href"

xpath_profile_links =\
    "//*[contains(@class,'secondary-pane')]"+\
    "/*[contains(@class,'user-profile')]"+\
    "/.//a[contains(@class,'yt-uix-redirect-link')]/@href"

def scrape_links(username,base_url=u"http://www.youtube.com/user/"):
    '''
    Returns a set of urls scraped off of a youtube user page

    It simply looks for the user profile within the pane on the right, and
    pulls the link elements from it.

    Alright, we're changing things up a bit here.

    We'll return a dictionary with context as key, set of links as value

    '''
    url = urlparse.urljoin(base_url, username)

    response = urllib2.urlopen(url)
    htmlparser = etree.HTMLParser()
    tree = etree.parse(response, htmlparser)

    links_with_context = dict()

    links_with_context['google_added_links'] = tree.xpath(xpath_gplus)
    links_with_context['profile_links'] = tree.xpath(xpath_profile_links)

    # Test for captcha-throttle or awkward page:
    throttled = len(tree.xpath("//*[contains(@class,'secondary-pane')]")) == 0
    links_with_context['throttled'] = throttled

    return links_with_context

def links_from_videos(username,
                      base_url=u"http://gdata.youtube.com/feeds/api/users/",
                      threshold=.08):
    '''

    Pulls the links from a video feed meeting a threshold of appearance.

    '''
    url = urlparse.urljoin(base_url, username, u"uploads")
    params = { 'max-results': 50 }
    param_str = urllib.urlencode(params)
    response = urllib2.urlopen(url, data=param_str)
    tree = etree.parse(response, etree.XMLParser())

    def url_tokens(entry):
        has_http = lambda text: "http" in text
        filtered = itertools.ifilter(has_http, unicode(content.text).split())
        return map(lambda s: s[s.index("http"):], filtered)

    ns = {"atom":"http://www.w3.org/2005/Atom"}
    contents = tree.xpath(u"//atom:entry/atom:content",namespaces=ns)

    vid_link_lists = [url_tokens(content) for content in contents]

    num_vids = float(len(vid_link_lists))
    # Remove duplicates within a single video
    # Originally used set then noticed the speedup of using groupby...
    # Almost makes me think set() should just use groupby...
    vid_link_deduped = ((k for k, _ in itertools.groupby(vid_link_list)) for
            vid_link_list in vid_link_lists)

    # Then we'll flatten this list of sets
    video_links = [link for link_set in vid_link_deduped for link in link_set]

    # And count them up
    counter = collections.Counter(video_links)

    # Then scale according to the length of vid_link_sets
    scale = lambda count: count/float(num_vids)
    scaled_urls = ((url, scale(count)) for url, count in counter.iteritems())

    # For a prolific "enough" youtuber, this feed generates
    # 25 videos (.08 == 2/25), so I'm looking for items appearing in at least
    # 3 videos for a prolific user
    above_threshold = lambda scaled_url: scaled_url[1] > threshold
    thresholded = set(filter(above_threshold, scaled_urls))

    links_with_context = dict()
    links_with_context['links_from_videos'] = thresholded
    return links_with_context


def scrape_user(username):
    return scrape_links(username, u"http://www.youtube.com/user/")

def scrape_channel(channel):
    return scrape_links(channel, u"http://www.youtube.com/channel/")

def scrape_guess(somename):
    '''
    If at first you don't succeed, try, except and then let Exceptions live out
    happy lives somewhere else
    '''
    links_with_context = dict()

    # Best check this heuristic first
    if(len(somename) >= 22):
        try:
            links_with_context.update(scrape_channel(somename))
        except:
            links_with_context.update(scrape_user(somename))
    else:
        # Likely a user, fall back on channel, then just let explosions happen
        try:
            links_with_context.update(scrape_user(somename))
        except:
            links_with_context.update(scrape_channel(somename))

    try:
        # Must have reached at least one type of page by now.
        # Time to rip links off some video pages
        links_with_context.update(links_from_videos(somename))
    except:
        # If that failed, oh well
        pass
    # If it succeeded, we just send back what we got
    return links_with_context


