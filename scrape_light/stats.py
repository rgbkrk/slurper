#!/usr/bin/env python

# -*- coding: utf-8 -*-

from sqlalchemy import and_, not_, or_
from sqlalchemy import func
from db import Link,YouTuber, create_session

import collections
import urllib2

import numpy
import matplotlib.pyplot as pyplot

def plot_counter(ctr):

    labels, freqs = [list(t) for t in zip(*ctr.most_common(20))]

    pos = numpy.arange(len(labels))
    width = 1.0
    ax = pyplot.axes()
    ax.set_xticks(pos + (width / 2))
    ax.set_xticklabels(labels)
    pyplot.bar(pos, freqs, width, color='r')
    pyplot.tight_layout(pad=2.5)
    pyplot.show()

# For link queries
facebook_links = or_( Link.url.ilike(u'%facebook.com%'),
                      Link.url.ilike(u'%fb.me%') )

twitter_links = Link.url.ilike(u'%twitter.com%')
tumblr_links = Link.url.ilike(u'%tumblr.com%')
want_links = or_(facebook_links, twitter_links, tumblr_links)
not_none_links = Link.url != None

gplus_links = Link.url.ilike(u'%plus.google.com%')
google_links = or_( Link.url.ilike(u'%google.com%'),
                    Link.url.ilike(u'%goo.gl%'))

# Captures webstagram, instagram, statigr.am
stagram_links = or_(Link.url.ilike(u'%stagram.com%'),
                    Link.url.ilike(u'%statigr.am'))

twitchtv_links = Link.url.ilike(u'%twitch.tv%')
bliptv_links = Link.url.ilike(u'%blip.tv%')
blogspot_links = Link.url.ilike(u'%blogspot.com%')
pinterest_links = Link.url.ilike(u'%pinterest.com%')
itunes_links = Link.url.ilike(u'%itunes.com%')

def has_link(link):
    '''
    Checks the profile links, google added links, and links from videos for
    `link`.
    '''
    return or_(
                YouTuber.gal.any(link),
                YouTuber.pl.any(link),
                YouTuber.lfv.any(link)
            )

def extract(url):
    try:
        nl = urllib2.urlparse.urlparse(url).netloc
        nl = nl.lower()
        if('' == nl):
            return None
        dom_split = nl.split('.')
        if(dom_split[-2] == 'co' or dom_split[-2] == 'com'):
            dom_split = dom_split[:-2]
        dom = ".".join(dom_split[-2:])

        domain_remapper = {
            u"fb.me"           : u"facebook.com",
            u"fb.com"          : u"facebook.com",
            u"goo.gl"          : u"google.com",
            u"gplus.to"        : u"google.com",
            u"youtu.be"        : u"youtube.com",
            u'inkstagram.com'  : u"instagram.com",
            u'stagram.com'     : u"instagram.com",
            u'www.instagram'   : u"instagram.com",
            u'pinstagram.co'   : u"instagram.com",
            u'statigr.am'      : u"instagram.com",
            u'amzn.to'         : u"amazon.com",
            u'vkontakte.ru'    : u"vk.com",
            u''      : u"",
            u''      : u"",
            u''      : u"",
            u''      : u"",
        }
        if(dom in domain_remapper):
            dom = domain_remapper[dom]

        return dom
    except Exception as e:
        print(e)
        print(url)
        return None

class Status():
    def __init__(self, Session=None):
        if(Session == None):
            Session = create_session()
        self.Session = Session

    def users_with_links(self, link, session=None):
        if(session is None):
            session = self.Session()
        return session.query(YouTuber).filter(has_link(link))

    def query_empties(self,session=None):
        if(session is None):
            session = self.Session()
        return session.query(YouTuber).filter(not_(has_link(not_none_links)))

    def domain_peek(self, session=None):
        if(session is None):
            session = self.Session()
        users = (user for user in session.query(YouTuber))
        links_per_user = (user.lfv + user.pl + user.gal for user in users)
        urls_per_user = [[link.url for link in links] for links in links_per_user]
        grouped_domains_by_user = (frozenset(map(extract,urls)) for urls in
                urls_per_user)

        counter = collections.Counter()

        for domains in grouped_domains_by_user:
            counter.update(domains)

        # Fix up some knowns...
        del counter[None]

        return counter, urls_per_user

    def lessers(self,session=None):
        print("\n== Top Usage ==\n")

        counter, urls_per_user = domain_peek(session)

        for domain,count in counter.most_common(25):
            print("{: >20}    {: >6}    {: >5.2f}%".format(domain, count, (count*100)/float(sum(counter.values()))))


    def summary(self):
        # Set up a session with the database for the summary
        session = self.Session()

        # YouTubers that list a Facebook account
        facebookers = self.users_with_links(facebook_links,session)
        # YouTubers that list a Twitter account
        tweeters = self.users_with_links(twitter_links,session)
        # YouTubers that list a Tumblr account
        tumblrs = self.users_with_links(tumblr_links,session)

        # YouTubers that list at least one of the wanted three
        wants = self.users_with_links(want_links,session)

        # Empty rows
        empties = self.query_empties(session)

        wantString = "\n%i YouTubers with at least one account listed from " +\
              "Facebook, Twitter, or Tumblr\n"
        print("== Summary ==")
        print(wantString % wants.count())

        print("%i Facebookers" % facebookers.count())
        print("%i Tweeters" % tweeters.count())
        print("%i Tumblrs" % tumblrs.count())

        print("\n%i YouTubers with no links" % empties.count())

        session.close()

    def sample(self,n):
        session = self.Session()
        return session.query(YouTuber).order_by(func.random()).limit(n)

    def rand_row(self):
        return self.sample(1).one()

if __name__ == "__main__":
    status = Status()
    status.summary()
    status.lessers()


