#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''

Extraction modules for pulling out the accounts based on URLs

'''

#TODO: Resolve fb.me

# For now, let's do some light extraction attempts and see how they perform.
import stats
import db
from db import YouTuber,TwitterAccount,TumblrAccount,FacebookAccount,Link
from sqlalchemy import func
from sqlalchemy import and_, not_, or_
import csv

import json
import httplib
import urllib2
from urllib2 import urlparse
import os
import re

from itertools import ifilter, imap
import functools
import random
import difflib
import time

import IPython.lib.pretty

import string

import logging
import operator

logging.basicConfig(filename='extraction.log', level=logging.DEBUG)

has_twitter = lambda link: "twitter." in link.url.lower()
has_facebook = lambda link: "facebook." in link.url.lower()
has_tumblr = lambda link: "tumblr." in link.url.lower()
has_gplus = lambda link: "plus.google" in link.url.lower()
has_blogspot = lambda link: "blogspot." in link.url.lower()

def can_retrieve(url, naptime=.40):
    '''
    Attempts to retrieve a URL. Returns True/False
    Pauses half a second + 0.0-0.99 seconds by default
    '''
    time.sleep(naptime + random.random())
    retrieved = False
    try:
        logging.debug("RETR: " + url)
        fd = urllib2.urlopen(url)
        logging.debug("RETR: " + str(fd.code))
        data = fd.read()
        # Check code here?
        retrieved = True
    except:
        logging.debug("RETR: FAILED")
    return retrieved

def checkUrl(url, naptime=.40):
    '''
    Attempts to see if a URL is valid. Returns True/False
    Pauses according to naptime

    Lets redirects pass.
    '''
    time.sleep(naptime + random.random())
    url_components = urlparse.urlparse(url)
    conn = httplib.HTTPConnection(url_components.netloc)

    full_path = url_components.path
    if(url_components.fragment != ''):
        full_path = full_path + "#" + url_components.fragment

    conn.request('HEAD', full_path)
    resp = conn.getresponse()
    print(resp.status)
    #return resp
    return resp.status < 400

def closest_name(username, names, check_urls):
    '''
    Returns the closest match to the given username across a list of names,
    while remaining case insensitive.

    >>> closest_name(u'wideframeofficial',
    ...              [u'AkaashVani', u'WIDEFRAMEPICTURES'],
    ...              u'https://graph.facebook.com/{}')
    u'WIDEFRAMEPICTURES'

    '''
    canon_names = {name.lower():name for name in names}
    # "Sort" the names in order of closeness to the username
    canon_users = difflib.get_close_matches(username.lower(), canon_names, cutoff=0.0)
    for canon_user in canon_users:
        user = canon_names[canon_user]
        for check_url in check_urls:
            url = check_url.format(user)
            if(can_retrieve(url)):
                return user

    user = None
    return user

def dedupe_and_find(tuber,has,extract,check_urls):
    tube_user = tuber.username
    user_pl = filter(has, tuber.pl)
    user_lfv = filter(has, tuber.lfv)
    user_gal = filter(has, tuber.gal)
    return dedupe_and_find_ind(tube_user,has,extract,check_urls,
                                user_pl, user_lfv, user_gal)

def dedupe_and_find_ind(tube_user,has,extract,check_urls,user_pl, user_lfv,
        user_gal):
    pl_urls = [link.url for link in (user_pl + user_gal)]
    lfv_urls = [(link.url,link.ratio) for link in user_lfv]

    pl = list(set(extract(url) for url in pl_urls))
    pl = filter(lambda x: x != None, pl)
    lfv = [(extract(url), ratio) for (url,ratio) in lfv_urls]
    lfv = filter(lambda x: x[0] != None, lfv)
    lfv2 = {}
    for user,ratio in lfv:
        if user in lfv2:
            lfv2[user] = max(lfv2[user], ratio)
        else:
            lfv2[user] = ratio
    lfv = lfv2.items()

    fuckups = filter(lambda x: ".php" in x, pl + [y[0] for y in lfv])
    if(len(fuckups) > 0):
        logging.debug("PHP"*10)
        logging.debug("PHP: " + str(tube_user))
        logging.debug("PHP: " + str(fuckups))

    user = None

    if(len(pl) == 1):
        # Found just one user account
        # This is the golden type
        user = pl[0]
    elif(len(pl) > 1):
        user = closest_name(tube_user, pl,check_urls)

    if(user is not None and user != '.'):
        pass
    elif(len(lfv) >= 1):
        user,ratio = max(lfv, key=lambda x: x[1])

        maxusers = [u[0] for u in lfv if u[1] == ratio]

        # What if there is more than one max?
        if(len(maxusers) > 1):
            user = closest_name(tube_user, maxusers,check_urls)

    else:
        pass

    if(user == '.'):
        user = None

    if(user != None):
        for check_url in check_urls:
            if(can_retrieve(check_url.format(user))):
                return user

    return None

def gplus_extract(url):
    '''
    >>> gplus_extract(u'https://plus.google.com/108780788478304965334')
    u'108780788478304965334'

    >>> gplus_extract(u'https://plus.google.com/u/0/108780788478304965334/posts')
    u'108780788478304965334'

    >>> gplus_extract(u'https://plus.google.com/u/0/stream#108780788478304965334/posts')
    u'108780788478304965334'

    >>> gplus_extract(u'https://plus.google.com/+TechnoBuffalo')
    u'TechnoBuffalo'

    >>> gplus_extract(u'https://plus.google.com/u/1/+%E9%99%B3%E7%8F%8A%E5%A6%AE/posts')
    u'陳珊妮'

    >>> gplus_extract(u'https://plus.google.com/s/d-block%20%26%20s-te-fan#115453463191335576067/posts')
    u'115453463191335576067'

    >>> gplus_extract(u'http://plus.google.com/u/0/#110245329749222903914/')
    u'110245329749222903914'

    >>> gplus_extract(u'https://plus.google.com/u/0/?tab=mX#114459691020873162210/posts')
    u'114459691020873162210'

    >>> gplus_extract(u'https://plus.google.com/?tab=wX&gpcaz=9339c32a#109816210098678627695/posts')
    u'109816210098678627695'

    >>> gplus_extract(u'https://plus.google.com/#113243281351788478914/posts')
    u'113243281351788478914'

    >>> gplus_extract(u'https://plus.google.com/s/T.M.Lewin#115596106811673197225/posts')
    u'115596106811673197225'

    >>> gplus_extract(u'https://plus.google.com/u/0/?tab=wX#102803574277489258123/posts')
    u'102803574277489258123'

    >>> gplus_extract(u'https://plus.google.com/+HM/posts')
    u'HM'

    >>> gplus_extract(u'http://plus.google.com/+RT')
    u'RT'

    >>> gplus_extract(u'https://plus.google.com/i/i7xgrXVPfuw:kSpnOpRiWo4')
    >>> gplus_extract(u'https://plus.google.com/u/0/me/posts?hl=en')
    >>> gplus_extract(u'https://plus.google.com/u/0/me/posts')
    >>> gplus_extract(u'https://plus.google.com/u/0/?gpsrc=gplp0')
    >>> gplus_extract(u'https://plus.google.com/u/0/')
    >>> gplus_extract(u'Google+:https://plus.google.com/u/0/')
    >>> gplus_extract(u'https://plus.google.com')


    '''


    ignores = "|".join([
            ur"/",
            ur"/u/1/",
            ur"/u/0/",
            ur"/u/0/me/posts",
            ur"/u/0/me/posts?hl=en",
            ur"/u/0/?gpsrc=gplp0",
            ur"/i/i7xgrXVPfuw:kSpnOpRiWo4",
            ur"",
            ])
    ignores = ignores.replace(ur"?", ur"\?")

    empties = re.compile(ur"^(?:https?://)?"
                         ur"(?:www.|m.|www.new.|ru-ru)?"
                         ur"plus.google.com"
                         ur"(?:"
                         + ignores +
                         ur")$",
                         flags=re.IGNORECASE)
    if(empties.match(url)):
        return None

    clean_url = url
    clean_url = clean_url.replace(unichr(0x200b), "")
    clean_url = clean_url.replace("//", "/")
    clean_url = urllib2.unquote(clean_url)

    gplus_regexes = [
        re.compile(
            ur'plus.google.com'
            ur'(?:/u/0/|/\w)*' # Weird tag
            ur'/#?(\d{18,})/?' # Should be 21 -- we'll have some leniency
            ,
            flags = re.IGNORECASE
        ),
        re.compile(
            ur'#?(\d{18,})'
            ,
            flags = re.IGNORECASE
        ),
        re.compile(
            ur'plus.google.com'
            ur'(?:/u/\d/|/\w)*' # Weird tag
            ur'/\+?(\w{2,})'
            ,
            flags = re.IGNORECASE
        ),
    ]

    found = imap(functools.partial(re.findall, string=clean_url), gplus_regexes)
    matches = ifilter(lambda x: x, found)

    for match in matches:
        user = match[-1]
        return user

    logging.debug("G"*80)
    logging.debug(url)
    logging.debug(clean_url)
    return None

def twitter_extract(url):
    '''
    Extracts a twitter account from a URL.

    >>> twitter_extract(u'http://twitter.com/domingo0022iway')
    u'domingo0022iway'

    >>> twitter_extract(u'https://twitter.com/#!/IEnergyIEditing')
    u'IEnergyIEditing'

    >>> est1 = twitter_extract(u'https://twitter.com/Estelle_Yuki')
    >>> est2 = twitter_extract(u'https://twitter.com/#!/Estelle_Yuki')
    >>> est1 == est2
    True

    '''

    twitter_regex = re.compile(
            ur'twitter.com'     # twitter.com is our anchor
            ur'(?:/.*#!|/.*#)?' # The AJAX crawl tag
            ur'/(?:@|#)?'       # Junk, silly users
            ur'([\w\-._]+)'     # The handle!
            ,
            flags=re.IGNORECASE
    )

    clean_url = url
    clean_url = clean_url.replace(unichr(0x200b),"")
    clean_url = clean_url.replace("//", "/")
    clean_url = urllib2.unquote(clean_url)

    matches = twitter_regex.findall(clean_url)
    if(len(matches) > 0):
        user = matches[0]
        return user

    # Handle the weird dash URLs
    twitter_regex2 = re.compile(twitter_regex.pattern.replace("/","-"),
            flags=re.IGNORECASE)
    matches = twitter_regex2.findall(clean_url)

    if(len(matches) > 0):
        user = matches[0]
        return user

    logging.debug("T"*20)
    logging.debug("TWIT: " + url)
    logging.debug("TWIT: " + clean_url)

    return None

def handle_twitter(session):
    twitterers = session.query(YouTuber).\
                 filter(stats.has_link(stats.twitter_links))

    twitterer_sample = twitterers

    for tuber in twitterer_sample:
        tuber.twitter_accounts = []
        try:
            user = dedupe_and_find(tuber, has_twitter, twitter_extract,
            ["https://www.twitter.com/{}"])
        except Exception as e:
            logging.debug("EXTRACTFAIL:" + unicode(tuber))
            logging.debug("EXTRACTFAIL:" + str(e))
            pass
        if(user != None and user != '.'):
            tuber.twitter_accounts = [TwitterAccount(user)]

    session.commit()

def handle_gplus(session):
    gplusers = session.query(YouTuber).\
            filter(stats.has_link(stats.google_links))

    #gplusers = gplusers[:10]

    fd = open("gplus.csv", "w")
    writer = csv.writer(fd, dialect='excel')
    writer.writerow(("youtuber","gplus"))

    for tuber in gplusers:
        user = dedupe_and_find(tuber, has_gplus, gplus_extract,
                                [u"https://plus.google.com/{}/",
                                 u"https://plus.google.com/+{}/"])
        if(user != None):
            writer.writerow((tuber.username, user))

    fd.close()

def blogspot_extract(url):
    '''
    Extracts a blogspot account from a URL
    '''
    reggie = re.compile("([^./]+)\.blogspot\.", flags=re.IGNORECASE)

    matches = reggie.findall(url)

    if(matches):
        user = matches[-1]
        return user

def handle_blogspot(session):
    blogspots = session.query(YouTuber).\
            filter(stats.has_link(Link.url.ilike(u"%blogspot%")))

    fd = open(u"blogspots.csv", u"w")
    writer = csv.writer(fd, dialect='excel')
    writer.writerow((u"youtuber",u"blogspot"))

    for tuber in blogspots:
        user = dedupe_and_find(tuber, has_blogspot, blogspot_extract,
                [u"http://{}.blogspot.com/"])
        if(user != None):
            writer.writerow((tuber.username, user))

    fd.close()


def tumblr_extract(url):
    '''
    Extracts a Tumblr account from a URL

    >>> tumblr_extract("http://razokev.tumblr.com/")
    u'razokev'

    >>> tumblr_extract("http://withkendra.tumblr.com/")
    u'withkendra'

    >>> tumblr_extract("http://merthu-r.tumblr.com/")
    u'merthu-r'

    >>> tumblr_extract("http://www.getnakedforasecnow.tumblr.com")
    u'getnakedforasecnow'

    '''
    url = url.lower()

    try:
        components = filter(lambda x: x != u'', url.split(u"/"))

        tumblrs = filter(lambda (ii,el): "tumblr.com" in el, enumerate(components))
        (domain_loc, domain) = tumblrs[0]

        dots = domain.split('.')
        tumbloc = dots.index(u"tumblr")
        if(tumbloc > 0 and dots[tumbloc-1] != "www"):
            return dots[tumbloc-1]

        # Now we need to look past domain_loc
        if(len(components) > domain_loc+1 and\
                components[domain_loc+1] != "blog"):
            return components[domain_loc+1]
        elif(len(components) > domain_loc + 2):
            return components[domain_loc + 2]
    except:
        # Quick hack for urls of the form usernametumblr.com
        # I am annoyed at the users that do this...
        x = filter(lambda p: "tumblr.com" in p, url.split(u"/"))
        x = x[0].split(".")
        x = filter(lambda p: "tumblr" in p, x)
        return x[0].split("tumblr")[0]

def handle_tumblr(session):
    tumblrers = session.query(YouTuber).\
                 filter(stats.has_link(stats.tumblr_links))

    tumblrer_sample = tumblrers

    for tuber in tumblrer_sample:
        tuber.tumblr_accounts = []
        user = dedupe_and_find(tuber, has_tumblr, tumblr_extract,
        ["http://{}.tumblr.com"])
        if(user != None and user != '.'):
            tuber.tumblr_accounts = [TumblrAccount(user)]
    session.commit()

def fb_graph(fb_id):
    FAILSAFE_TOKEN = (
            "AAACEdEose0cBADBOzc2ivwNzWGn69dD7TeYaZAZA02O1XzHqsDzmIpcL9Lwz4m6gxOD3RPOUgvAHPKpxaN3BmfvqsjDE9LoCcAExVZC1gZDZD"
                    )
    param_str = (
            'client_secret=dd5938ff283509d193ec29273e4330b0'
            '&grant_type=client_credentials'
            '&client_id=108652385839586'
            )
    try:
        at = urllib2.urlopen("https://graph.facebook.com/oauth/access_token",
                         data=param_str).read()
        if(at.startswith('access_token=')):
            FB_TOKEN = at[len('access_token='):]
        else:
            FB_TOKEN = FAILSAFE_TOKEN
    except:
        FB_TOKEN = FAILSAFE_TOKEN

    try:
        url = "https://graph.facebook.com/" + fb_id + "/?access_token=" + FB_TOKEN
        graph_call = url
        fd = urllib2.urlopen(graph_call)
        data = fd.read()
    except:
        FB_TOKEN = FAILSAFE_TOKEN
        url = "https://graph.facebook.com/" + fb_id + "/?access_token=" + FB_TOKEN
        graph_call = url
        fd = urllib2.urlopen(graph_call)
        data = fd.read()

    return json.loads(data)


def facebook_extract(url):
    '''

    Extracts the likely account associated with a facebook link.

    >>> facebook_extract(u'https://www.facebook.com/dabrozzpage/app_153284594738391')
    u'dabrozzpage'

    >>> facebook_extract(u'http://www.facebook.com/dabrozzpage')
    u'dabrozzpage'

    >>> facebook_extract(u'http://www.facebook.com/profile.php?id=100003589042890')
    u'100003589042890'

    #>>> facebook_extract(u"http://www.facebook.com/photo.php?fbid=480039308714386")
    #u'480039308714386' CHECK ME

    >>> facebook_extract(u"http://www.facebook.com/group.php?gid=21149051062"
    ...                  u"&ref=ts")
    u'21149051062'

    >>> facebook_extract(u"http://www.facebook.com/profile.php?"
    ...                  u"ref=profile&id=100000940501443#!/profile.php?ref=profile&id=100000940501443")
    u'100000940501443'

    >>> facebook_extract(u"http://www.facebook.com/home.php?sk="
    ...                  u"group_168503473182809")
    u'168503473182809'

    >>> facebook_extract(u"http://www.facebook.com/permalink.php?story_fbid=250380838400768"
    ...                  u"&id=100001630659972&notif_t=share_comment#!/pages/Cynthia-Dulude/200711649941410")
    u'200711649941410'

    >>> facebook_extract(u"https://www.facebook.com/aodhan.scott?ref=tn_tnmn#!/P3nAlPineapple")
    u'P3nAlPineapple'

    >>> facebook_extract(u"http://www.Facebook.com/LionFame")
    u'LionFame'

    >>> facebook_extract(u"http://Facebook.com/MurkAvenue")
    u'MurkAvenue'

    >>> facebook_extract(u"http://www.facebook.com/#!/juuuulliiee")
    u'juuuulliiee'

    >>> facebook_extract(u"http://www.facebook.com/photo.php"
    ...                  u"?fbid=181824805177702"
    ...                  u"&set=a.155736127786570.33939.100000505515454#!/")
    u'100000505515454'

    >>> facebook_extract(u"http://www.facebook.com/#/lisa.rado?ref=profile")
    u'lisa.rado'

    >>> facebook_extract(u'http://www.youtube.com/redirect?q='
    ...                  u'http%3A%2F%2Fwww.facebook.com%2Fgroup.php%3F'
    ...                  u'gid%3D257985172174&session_token='
    ...                  u'2aElubuarydydtJQNRQr1YEdi-V8MTI4NDY4MDY3MA%3D%3D')
    u'257985172174'

    >>> facebook_extract(u'http://cf.addthis.com/red/ct.html?gen=2000&ev='
    ...                  u'sw_facebook&rb=0&pco=cnv-100&dspid=6&cid=302551&clk='
    ...                  u'https%3A%2F%2Fwww.facebook.com%2F'
    ...                  u'snowwhiteandthehuntsman')
    u'snowwhiteandthehuntsman'


    >>> facebook_extract(u'http://www.facebook.com/'
    ...                  u'event.php?eid=227957113898847')
    u'789314479'

    The following user is actually the one on the left, and not the owner
    of the event...

    #>>> facebook_extract(
    #... u'http://www.facebook.com/robynnyip#!/event.php?eid=187491614596355'
    #... )
    #u'robynnyip'

    Facebook "pages" should return the ID, as the name doesn't work
    Facebook page names apparently aren't unique

    >>> facebook_extract('https://www.facebook.com/pages/'
    ...                  'FabyGlam/359685304080101')
    u'359685304080101'

    >>> facebook_extract(u'http://www.facebook.com//Ey3shi3ld')
    u'Ey3shi3ld'

    This almost made me wonder if youtube was throttling me
    u200b == utwo oo b == YouTube

    >>> facebook_extract(u'https://www.facebook.com/\u200bDeirAlZorPN')
    u'DeirAlZorPN'

    >>> facebook_extract(u'https://www.facebook.com/'
    ...                  u'\u200bDeirPressNetworkEnglish')
    u'DeirPressNetworkEnglish'

    >>> facebook_extract(u'https://www.facebook.com//waelelebrashy')
    u'waelelebrashy'

    >>> facebook_extract(u'http://www.facebook.com.br/tomateofficial')
    u'tomateofficial'

    >>> facebook_extract(u'http://www.facebook.com.au/arb4x4')
    u'arb4x4'

    >>> facebook_extract(u'http://www.facebook.comEsteShowApesta')
    u'EsteShowApesta'

    We should call out to find the note owner

    #>>> facebook_extract(u'https://www.facebook.com/note.php?'
    #...                  u'note_id=210277189000533')
    #u'100000547595058'

    #>>> facebook_extract(u'https://www.facebook.com/note.php?'
    #...                  u'note_id=209553142406271')
    #u'KILL ME'

    >>> facebook_extract(u'https://www.facebook.com/note.php?'
    ...                  u'note_id=287231894682729')
    u'146241832115070'

    >>> facebook_extract(u'http://www.facebook.com/search.php?'
    ...                  u'q=faby+glam&init=quick&tas=search')
    u'359685304080101'

    >>> facebook_extract(u'https://www.facebook.com/photo.php?'
    ...                  u'v=286413378139256')
    u'210962049017723'

    Now for the users that either don't understand the internet or are
    using their YouTube page as a bookmarks folder.

    >>> facebook_extract(u'http://www.facebook.com')
    >>> facebook_extract(u'http://www.facebook.com/profile.php?id=')
    >>> facebook_extract(u'http://www.facebook.com/profile.php?')
    >>> facebook_extract(u'http://www.facebook.com/home.php')
    >>> facebook_extract(u'http://www.facebook.com/reqs.php#!/?ref=logo')

    '''

    ignores = "|".join([
            ur"/profile.php",
            ur"/profile.php?id=",
            ur"/profile.php?i...",
            ur"/reqs.php#!/?ref=logo",
            ur"/#!/",
            ur"/editaccount.php?networks",
            ur"/update_security_info.php?wizard=1",
            ur"/?re..",
            ur"/?ref=home",
            ur"/?ref=hp",
            ur"/?",
            ur"/?ref=hpskip",
            ur"/?ref=tn_tnmn",
            ur"/?sk=media",
            ur"/?sk=welcome",
            ur"/?email_confirmed=1",
            ur"/?ref=logo",
            ur"/group.php?gid...",
            ur"/groups.php?ref=sb",
            ur"/home.php?filter=lf",
            ur"/home.php?ref=home",
            ur"/home.php?ref=hp",
            ur"/home.php?ref=logo",
            ur"/home.php?sk=lf",
            ur"/home.php",
            ur"/home.php?",
            ur"/share.php?u=http://www.youtube.com",
            ur"/l.php?u=http%...",
            ur"/?_rdr",
            ur"/",
            ur"/reqs.php",
            ur"/pages-create.php",
            ur"&_rdr",
            ur"/profile.php?r...",
            ur"/?ref=tn_tinyman",
            ur"",
            ur"",
            ur"",
            ])
    ignores = ignores.replace(ur"?", ur"\?")

    #TODO: Add in doctests for all the regexes
    empties = re.compile(ur"^(?:https?://)?"
                         ur"(?:www.|m.|www.new.|ru-ru)?"
                         ur"facebook.com?"
                         ur"(?:"
                         + ignores +
                         ur")\??$",
                         flags=re.IGNORECASE)
    if(empties.match(url)):
        return None

    fb_name = ur"[\w\-._]+"
    GETS = ur"(?:\?|\&)"

    id_field = GETS + ur"{}=(\d+)"
    skid_field = GETS + ur"sk=group_(\d+)"
    folder_field = ur"/{}/(" + fb_name + ur")/"

    page_field = ur"/pages/(" + fb_name + ur")(?:/(\d+))?" # Prefer ids

    name_field = ur"facebook\.com/(" + fb_name + ")"

    fb_id_regex = re.compile(id_field.format(u"id"),
            flags=re.IGNORECASE)
    fb_gid_regex = re.compile(id_field.format(u"gid"),
            flags=re.IGNORECASE)
    skid_regex = re.compile(skid_field, flags=re.IGNORECASE)

    pages_regex = re.compile(page_field, flags=re.IGNORECASE)

    people_regex = re.compile(folder_field.format(u"people"),
            flags=re.IGNORECASE)
    groups_regex = re.compile(folder_field.format(u"groups"),
            flags=re.IGNORECASE)
    photo_set_regex = re.compile(GETS + "set=\w+.\w+.\w+.(\w+)",
            flags=re.IGNORECASE)
    named_regex = re.compile(ur"facebook\.com/(" + fb_name + ")",
            flags=re.IGNORECASE)

    regexes = [
        fb_id_regex,
        fb_gid_regex,
        skid_regex,
        people_regex,
        groups_regex,
        photo_set_regex,
        named_regex
    ]

    clean_url = urllib2.unquote(url)
    clean_url = clean_url.replace(unichr(0x200b), u"")


    if("facebook.com-" in clean_url.lower()):
        clean_url = url.replace("facebook.com-","facebook.com/")
        return facebook_extract(clean_url)


    # Alternate TLDs
    clean_url = re.sub(ur"facebook.com\.[a-zA-Z0-9]+/", "facebook.com/",
                        clean_url,
                        flags=re.IGNORECASE)

    if(u"facebook.com//" in clean_url.lower()):
        return facebook_extract(clean_url.replace(u"facebook.com//",
            u"facebook.com/"))

    matcher = re.search(ur"facebook.com([^/])", clean_url, flags=re.IGNORECASE)
    if(u"facebook.com/" not in clean_url and matcher):
        clean_url = re.sub(ur"facebook.com([^/])",
                           ur"facebook.com/\1",
                           clean_url)

    if(ur"#!/" in url or ur"#/" in url):
        if(ur"#!/" in url):
            bangs = url.split(ur"#!/")
        elif(ur"#/" in url):
            bangs = url.split(ur"#/")
        if(len(bangs[-1]) > 1):
            right_url = urlparse.urljoin("http://www.facebook.com/",bangs[-1])
            left_url = bangs[0]
            left_user = None
            right_user = None
            try:
                left_user = facebook_extract(left_url)
            except:
                pass
            try:
                right_user = facebook_extract(right_url)
            except:
                pass

            if(right_user is not None):
                return right_user
            return left_user


    finder = functools.partial(re.findall, string=clean_url)
    found = imap(finder, regexes)
    matches = ifilter(lambda x: x, found)

    page_matches = re.findall(pages_regex, clean_url)
    if(page_matches):
        page_name,page_id = page_matches[-1]
        if(page_id != ''):
            return page_id
        # TODO Make a Facebook API call to get the ID
        return page_name

    for match in matches:
        user = match[-1]
        if(".php" in user):
            continue
        return user

    # These are all the tries that look up the owners of events, photos, and
    # notes. If the link was a search (you're kidding me, right?), we perform
    # the search through the graph API and choose the page with the most
    # likes...
    if("event.php" in url.lower()):
        event_id_regex = re.compile(id_field.format(u"eid"),
                flags=re.IGNORECASE)
        match = event_id_regex.findall(url)
        if(match):
            eventID = match[-1]
            try:
                event_info = fb_graph(eventID)
                user = event_info['owner']['id']
                return user
            except Exception as e:
                logging.debug("Event lookup failed")
                logging.debug("EVT: " + url)
                logging.debug("EVT: " + str(e))
    elif("photo.php" in url.lower()):
        photo_id_regex = re.compile(id_field.format(u"v"),
                flags=re.IGNORECASE)
        match = photo_id_regex.findall(url)
        if(match):
            photoID = match[-1]
            try:
                photo_info = fb_graph(photoID)
                user = photo_info['from']['id']
                return user
            except Exception as e:
                logging.debug("Photo lookup failed")
                logging.debug("PHOTO: " + url)
                logging.debug("PHOTO: " + str(e))
                pass
    elif("note.php" in url.lower()):
        note_id_regex = re.compile(id_field.format(u"note_id"),
                flags=re.IGNORECASE)
        match = note_id_regex.findall(url)
        if(match):
            noteID = match[-1]
            try:
                note_info = fb_graph(noteID)
                user = note_info['from']['id']
                return user
            except:
                pass
    elif("search.php" in url.lower()):
        # Pull the query
        query = re.compile("q=([^&]+)", flags=re.IGNORECASE)
        matches = query.findall(url)
        match = matches[-1]

        try:
            #TODO: Make safe
            fd = urllib2.urlopen(ur"https://graph.facebook.com/search?type=page&"
                                 ur"q=" + match)
            results = json.loads(fd.read())
            data = results['data']

            big_hit = max(data, key=get_likes)
            return big_hit['id']
        except Exception as e:
            logging.debug("Search failed")
            logging.debug("SEARCH: " + url)
            logging.debug("SEARCH: " + str(e))
            return None


    logging.debug(u"FAILBOOK: " + url)
    logging.debug(u"FAILBOOK: " + clean_url)

    return None

def get_likes(search_entry):
    fb_id = search_entry['id']
    fd = urllib2.urlopen(
            ur"https://graph.facebook.com/" +
            fb_id
            )
    results = json.loads(fd.read())
    return int(results['likes'])

def handle_facebook(session):
    facebookers = session.query(YouTuber).\
                    filter(stats.has_link(stats.facebook_links))

    for tuber in facebookers:
        tuber.facebook_accounts = []
        try:
            user = dedupe_and_find(tuber, has_facebook, facebook_extract,
                    ["https://graph.facebook.com/{}"])
        except UnicodeEncodeError as uee:
            logging.debug("U"*25)
            logging.debug(uee)
            logging.debug(pretty.pretty(tuber))
            logging.debug(pretty.pretty(tuber.lfv))
            logging.debug(pretty.pretty(tuber.pl))
        except Exception as eek:
            logging.debug("EEEEEEEK")
            logging.debug(eek)
            logging.debug(pretty.pretty(tuber))
            logging.debug(pretty.pretty(tuber.lfv))
            logging.debug(pretty.pretty(tuber.pl))
    #    if(user != None and user != ''):
    #        tuber.facebook_accounts = [FacebookAccount(user)]
    #session.commit()

if __name__ ==  "__main__":
    logging.info("Starting session")
    Session = db.create_session()
    session = Session()
    #handle_twitter(session)
    #handle_facebook(session)
    #handle_tumblr(session)
    #handle_gplus(session)
    handle_blogspot(session)
    logging.info("Session done")


