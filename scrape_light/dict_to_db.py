#!/usr/bin/env python

# -*- coding: utf-8 -*-

import db
from db import YouTuber, Link, create_session
from sqlalchemy.orm.exc import NoResultFound

def go(tubers,loc,echo=False):
    Session = db.create_session(loc,echo=echo)
    session = Session()

    for username, link_dict in tubers.iteritems():
        create_update(username, link_dict, session)

def merge(old_urls, new_urls):
    old_urls.extend(new_urls.difference(old_urls))

def create_update(username, link_dict, session):
    has_any = link_dict.has_key('profile_links') or\
                link_dict.has_key('google_added_links') or\
                link_dict.has_key('links_from_videos')

    # If we didn't get anything in the dict, don't bother updating
    if(not(has_any)):
        return

    try:
        # Get one if it exists
        yt = session.query(YouTuber).filter(YouTuber.username == username).one()
    except NoResultFound as nrf:
        # Otherwise we'll make a new one
        yt = db.YouTuber(username)

    if(link_dict.has_key('profile_links')):
        merge(yt.profile_links, link_dict['profile_links'])
    if(link_dict.has_key('google_added_links')):
        merge(yt.google_added_links, link_dict['google_added_links'])
    if(link_dict.has_key('links_from_videos')):
        merge(yt.links_from_videos, link_dict['links_from_videos'])


    try:
        session.add(yt)
        session.commit()
    except Exception as e:
        print(e)
        session.rollback()
