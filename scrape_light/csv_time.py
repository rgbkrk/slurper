#!/usr/bin/env python

# -*- coding: utf-8 -*-

import db
import string
import codecs
import csv

def ex(tuber):
    return (
            tuber.username,
            "" if len(tuber.twitter_accounts) == 0 else tuber.twitter_accounts[0].user_id,
            "" if len(tuber.tumblr_accounts) == 0 else tuber.tumblr_accounts[0].user_id,
            "" if len(tuber.facebook_accounts) == 0 else tuber.facebook_accounts[0].user_id
            )

Session = db.create_session()
session = Session()

fd = open("social_youtubers.csv", "w")

writer = csv.writer(fd, dialect='excel')

writer.writerow(("youtuber","twitter_account","tumblr_account","facebook_account"))

for tuber in session.query(db.YouTuber):
    try:
        row = tuple(map(lambda x: x.strip(), ex(tuber)))
        writer.writerow(row)
    except UnicodeEncodeError as uee:
        print(tuber)
        print(ex(tuber))
        print(uee)

fd.close()

