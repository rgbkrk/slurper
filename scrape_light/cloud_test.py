#!/usr/bin/env python

# -*- coding: utf-8 -*-
'''
This is mostly just me tinkering with simulation in order to set up unit and
functional tests.

'''

simulate = True

import cloud_proc

if(simulate):
    cloud_proc.cloud.start_simulator()
    assert cloud_proc.cloud.is_simulated()

import db
import stats
import random
from users import youtubers
import scrape

from IPython.lib.pretty import pprint

tube_sample = []
#tube_sample.extend(random.sample(youtubers, 60))
tube_sample.append(u'vis8n')
tube_sample.append(u'lindseystomp')
tube_sample.append(u'fahadahmad')

num_proc = 2
split = len(tube_sample)/num_proc

all_tubes, all_errs, all_empties, jids = cloud_proc.split_scrape(tube_sample,
        split, scrape.links_from_videos)

pprint(all_tubes)

assert len(tube_sample) == len(all_tubes)
assert set(tube_sample) == set(all_tubes.keys())

print("Errors:")
pprint(all_errs)

print("Empties:")
pprint(all_empties)

print("jids:")
pprint(jids)

