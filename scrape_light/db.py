#!/usr/bin/env python

# -*- coding: utf-8 -*-
'''
This is just a simple set of tables for dealing with extracted URLs from
YouTube user and video pages.

>>> Session = create_session(db_location=":memory:", echo=False)
>>> session = session()
>>> youtuber = YouTuber(u"lindseystomp")
>>> links = [ # doctest: +NORMALIZE_WHITESPACE
... u'http://lindseystirlingviolin.com/tour/',\
... u'http://www.lindseystirlingviolin.com',\
... u'https://twitter.com/LindseyStirling',\
... u'http://www.facebook.com/pages/Lindsey-Stirling/132255980139931'\
... ]
>>> youtuber.profile_links = links
>>> session.add(youtuber)
>>> session.commit()
>>> session.query(YouTuber).all()
... [<YouTuber(u"lindseystomp")>]

'''

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String, DateTime, Unicode, Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.associationproxy import association_proxy

Base = declarative_base()

class YouTuber(Base):
    '''
    YouTuber keeps track of YouTube accounts by username.

    It is derived from a SQLAlchemy declarative base

    >>> tuber = YouTuber(u"Google")
    >>> tuber
    <YouTuber(u"Google")>
    >>> tuber.profile_links.append(ProfileLink("http://www.google.com"))
    >>> tuber.profile_links
    [<ProfileLink(u"http://www.google.com")>]

    '''
    __tablename__ = "youtubers"
    id = Column(Integer, primary_key=True)
    username = Column(Unicode,unique=True)

    # These all get proxied since the relationships are so simple
    pl = relationship("ProfileLink", back_populates='youtuber')
    profile_links = association_proxy('pl', 'profile_links')

    gal = relationship("GoogleAddedLink", back_populates='youtuber')
    google_added_links = association_proxy('gal', 'google_added_links')

    lfv = relationship("LinkFromVideo", back_populates='youtuber')
    links_from_videos = association_proxy('lfv', 'links_from_videos',
            creator=lambda tup: LinkFromVideo(tup[0], tup[1]))

    # These all get proxied since the relationships are so simple
    twitter_accounts = relationship("TwitterAccount", back_populates='youtuber')
    tumblr_accounts = relationship("TumblrAccount", back_populates='youtuber')
    facebook_accounts = relationship("FacebookAccount", back_populates='youtuber')


    def __init__(self,username):
        self.username = unicode(username)

    def __repr__(self):
        return "<YouTuber(%r)>" % self.username


class Link(Base):
    '''
    Link is just a URL to derive other classes from.

    It is derived from a SQLAlchemy declarative base

    '''
    __tablename__ = "links"
    id = Column(Integer, primary_key=True)
    url = Column(Unicode, nullable=False)

    def __init__(self, url):
        self.url = unicode(url)

    def __repr__(self):
        return "<Link(%r)>" % self.url

class ProfileLink(Link):
    '''
    >>> link = ProfileLink(u"http://www.google.com")
    >>> link
    <ProfileLink(u"http://www.google.com")>
    >>> tuber = YouTuber("Google")
    >>> tuber.profile_links.append(link)
    >>> tuber.profile_links
    [<ProfileLink(u"http://www.google.com")>]
    '''
    __tablename__ = "profile_links"
    id = Column(Integer, ForeignKey('links.id'), primary_key=True)

    youtuber_id = Column(Integer, ForeignKey('youtubers.id'))
    youtuber = relationship("YouTuber",
                            back_populates='pl',
                            order_by=id)
    def __repr__(self):
        return "<ProfileLink(%r)>" % self.url

class GoogleAddedLink(Link):
    __tablename__ = "google_added_links"
    id = Column(Integer, ForeignKey('links.id'), primary_key=True)

    youtuber_id = Column(Integer, ForeignKey('youtubers.id'))
    youtuber = relationship("YouTuber",
                            back_populates='gal',
                            order_by=id)
    def __repr__(self):
        return "<GoogleAddedLink(%r)>" % self.url

class LinkFromVideo(Link):
    __tablename__ = "links_from_videos"
    id = Column(Integer, ForeignKey('links.id'), primary_key=True)
    ratio = Column(Float, nullable=False)

    youtuber_id = Column(Integer, ForeignKey('youtubers.id'))
    youtuber = relationship("YouTuber",
                            back_populates='lfv',
                            order_by=id)

    def __init__(self, url, ratio):
        self.url = unicode(url)
        self.ratio = ratio

    def __repr__(self):
        return "<LinkFromVideo(%r,%r)>" % (self.url,self.ratio)

class SocialAccount(Base):
    __tablename__ = "social_accounts"
    id = Column(Integer, primary_key=True)
    user_id = Column(Unicode, nullable=False)

    def __init__(self, user_id):
        self.user_id = unicode(user_id)

    def __repr__(self):
        return "<%r(%r)>" % (self.__class__.__name__, self.user_id)

class TwitterAccount(SocialAccount):
    __tablename__ = "twitter_accounts"
    id = Column(Integer, ForeignKey('social_accounts.id'), primary_key=True)
    youtuber_id = Column(Integer, ForeignKey('youtubers.id'))
    youtuber = relationship("YouTuber",
                            back_populates='twitter_accounts',
                            order_by=id)
class TumblrAccount(SocialAccount):
    __tablename__ = "tumblr_accounts"
    id = Column(Integer, ForeignKey('social_accounts.id'), primary_key=True)
    youtuber_id = Column(Integer, ForeignKey('youtubers.id'))
    youtuber = relationship("YouTuber",
                            back_populates='tumblr_accounts',
                            order_by=id)

class FacebookAccount(SocialAccount):
    __tablename__ = "facebook_accounts"
    id = Column(Integer, ForeignKey('social_accounts.id'), primary_key=True)
    youtuber_id = Column(Integer, ForeignKey('youtubers.id'))
    youtuber = relationship("YouTuber",
                            back_populates='facebook_accounts',
                            order_by=id)

def create_session(db_location="sqlite3.db", echo=False):
    '''
    Creates a bound session maker

    >>> Session = create_session(db_location=":memory:", echo=False)
    >>> Session
    sqlalchemy.orm.session.SessionMaker
    >>> session = Session()
    >>> session.query(YouTuber).count()
    0

    '''
    engine = create_engine('sqlite:///%s' % db_location, echo=echo)
    Base.metadata.create_all(engine)
    return sessionmaker(bind=engine)

